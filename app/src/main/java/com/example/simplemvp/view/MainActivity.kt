package com.example.simplemvp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.simplemvp.R
import com.example.simplemvp.databinding.ActivityMainBinding
import com.example.simplemvp.model.SimpleModel
import com.example.simplemvp.presenter.MainView
import com.example.simplemvp.presenter.Presenter

class MainActivity : AppCompatActivity(), MainView {
    private lateinit var binding: ActivityMainBinding
    private lateinit var presenter: Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        presenter = Presenter(this, SimpleModel(0, 0))
        binding.btnAddText.setOnClickListener { //VIEW
            //logic to add a text and display to textview
            //displayToTextView()
            presenter.addText(binding.edtText.text.toString(), 2, 5)
        }

    }

    fun displayToTextView() {
        binding.progressBar.isVisible = true
        binding.txtResult.text = binding.edtText.text.toString() //CONTROLLER
        binding.progressBar.isVisible = false
    }

    override fun showLoading() {
        binding.progressBar.isVisible = true
    }

    override fun hideLoading() {
        binding.progressBar.isVisible = false
    }

    override fun updateText(text: String, a: Int) {
        binding.txtResult.text = "${text} $a"
    }

}