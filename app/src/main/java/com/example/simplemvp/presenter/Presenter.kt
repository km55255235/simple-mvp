package com.example.simplemvp.presenter

import com.example.simplemvp.model.SimpleModel

class Presenter(val view: MainView, val model: SimpleModel) {
    //STORE LOGIC BETWEEN VIEW AND MODEL

    fun addText(text: String, numA: Int, numB: Int){
        view.showLoading()
        model.a = numA
        model.b = numB
        view.updateText(text, model.a + model.b)
        view.hideLoading()
    }
}

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun updateText(text: String, calculatedResult: Int)
}